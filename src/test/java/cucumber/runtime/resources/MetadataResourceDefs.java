package cucumber.runtime.resources;

import com.abundance.pdf.runtime.resources.MetadataResource;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import javax.ws.rs.core.Response;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.fail;

public class MetadataResourceDefs {

    MetadataResource metadataResource;
    Response response;
    MetadataResource.Metadata metadata;

    @Given("the MetadataResource")
    public void theMetadataResource() {
        metadataResource = new MetadataResource();
    }

    @When("I fetch the field data")
    public void IFetchTheFieldData() {
        response = metadataResource.get();
    }

    @Then("the response contains Metadata")
    public void theResponseContainsMetadata() {
        Object entity = response.getEntity();
        assertNotNull("entity", entity);
        if (entity instanceof MetadataResource.Metadata) {
            metadata = (MetadataResource.Metadata) entity;
        } else {
            fail("Response is not MetadataResource.Metadata, it is: " + entity.getClass());
        }
    }

    @Then("the fields match what is in the Package information")
    public void theFieldsMatchWhatIsInThePackageInformation() {
        Package pkg = MetadataResource.class.getPackage();
        assertEquals(pkg.getImplementationTitle(), metadata.title);
        assertEquals(pkg.getImplementationVendor(), metadata.vendor);
        assertEquals(pkg.getImplementationVersion(), metadata.version);
    }

}
