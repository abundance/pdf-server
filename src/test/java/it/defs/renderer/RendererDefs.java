package it.defs.renderer;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import it.util.APIClient;
import it.util.HTTPMethod;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import static junit.framework.Assert.assertEquals;

public class RendererDefs {

    enum InputType { CERTIFICATE }

    APIClient api;

    String template;

    @Given("^the API (http[s]?://.*)$")
    public void the_API_url(String url) {
        api = new APIClient(url);
    }
    @Given("^the API defined in the \"([^\"]+)\" Java system property$")
    public void the_API_system_property(String key) {
        api = new APIClient(System.getProperty(key));
    }

    @Given("^the input: (.*)$")
    public void the_input(InputType input) {
        switch (input) {
            case CERTIFICATE : {
                StringBuilder buf = new StringBuilder();
                buf.append("<html><head>");
                buf.append("<style>body { font-size: 3em; }</style>");
                buf.append("</head><body>");
                buf.append("Hello World!");
                buf.append("</body></html>");

                template = buf.toString();
            }
        }
    }

    @When("^I generate the PDF$")
    public void I_generate_the_PDF() {
        api.execute(HTTPMethod.POST, "/template/render", template);
    }

    @Then("^I get the PDF I expected$")
    public void I_get_the_PDF_I_expected() throws URISyntaxException, IOException {
        InputStream expected = getClass().getClassLoader().getResourceAsStream("files/certificate.pdf");
        InputStream actual = api.getResponseEntity(InputStream.class);

        byte[] expectedBytes = IOUtils.toByteArray(expected);
        byte[] actualBytes = IOUtils.toByteArray(actual);
        assertEquals(expectedBytes.length, actualBytes.length);

        // TODO: assertArrayEquals(expectedBytes, actualBytes);
        // This keeps failing - seems as if no two PDFs are the same?
    }

    @Then("^I get the response code (\\d+)$")
    public void I_get_the_response_code(int code) {
        api.assertResponseStatus(code);
    }

    @Then("^I have notified an Administrator$")
    public void I_have_notified_an_Administrator() {
        throw new UnsupportedOperationException("TODO"); // TODO
    }

}
