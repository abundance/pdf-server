package it.util;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.URLConnectionClientHandler;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import static junit.framework.Assert.assertEquals;

public class APIClient {

    String url;
    Client client;
    ClientResponse response;

    public APIClient(String url) {
        this.url = url;

        ClientConfig cc = new DefaultClientConfig();
        cc.getClasses().add(JacksonJsonProvider.class);
        client = new Client(new URLConnectionClientHandler(), cc);
        // client.addFilter(new com.sun.jersey.api.client.filter.LoggingFilter());
    }

    // Internal

    protected WebResource.Builder buildRequest(String path) {
        return client.resource(url + path)
                .type("application/json")
                .accept("application/json");
    }

    // External

    public void execute(HTTPMethod method, String path, Object data) {
        switch (method) {
            case POST : {
                response = buildRequest(path).post(ClientResponse.class, data);
                break;
            }
            default : throw new UnsupportedOperationException("Cannot execute "+ method +" for the path "+ path);
        }
    }

    public void assertResponseStatus(int code) {
        assertEquals(code, response.getStatus());
    }

    public <T> T getResponseEntity(Class<T> type) {
        return response.getEntity(type);
    }

}
