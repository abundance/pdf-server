Feature: Output Project Metadata
  In order for an actor to get the project metadata
  As a REST user
  I should be able to extract the information

  Scenario: Get Metadata
    Given the MetadataResource
    When I fetch the field data
    Then the response contains Metadata
    Then the fields match what is in the Package information
