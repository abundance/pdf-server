Feature: Render HTML to PDF over REST
  In order for an actor to get a PDF
  As a REST user
  I should be able to able to upload my resources and get them back transformed in PDFs

  Background:
    Given the API defined in the "pdf-server.url" Java system property

  Scenario: Certificate to PDF
    Given the input: CERTIFICATE
    When I generate the PDF
    Then I get the response code 200
    Then I get the PDF I expected
