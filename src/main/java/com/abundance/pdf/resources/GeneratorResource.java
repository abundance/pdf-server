package com.abundance.pdf.resources;

import com.lowagie.text.DocumentException;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.resource.FSEntityResolver;
import org.xml.sax.SAXException;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;

@Path("/template")
@Produces({"application/xml", "application/json"})
@SuppressWarnings("unused")
public class GeneratorResource {

    @POST
    @Path("/render")
    public Response templateRender(String template) throws IOException, DocumentException,
            ParserConfigurationException, SAXException
    {
        // Process the HTML into an XML document
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        builder.setEntityResolver(FSEntityResolver.instance());
        Document doc = builder.parse(new ByteArrayInputStream(template.getBytes()));

        // Render to PDF
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocument(doc, null);
        renderer.layout();
        renderer.createPDF(outputStream);

        outputStream.close();

        // Return the PDF
        return Response.ok(new ByteArrayInputStream(outputStream.toByteArray()))
                .type("application/pdf")
                .build();
    }

}
