package com.abundance.pdf.runtime.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;

@Path("/metadata")
@Consumes({"application/json"})
@Produces({"application/json"})
@SuppressWarnings("unused")
public class MetadataResource {

    final Metadata metadata;

    public MetadataResource() {
        Package packageMetadata = getClass().getPackage();
        metadata = new Metadata(packageMetadata);
    }

    @GET
    @Path("/")
    public Response get() {
        return Response.ok(metadata).build();
    }

    // Inner Classes

    @XmlRootElement
    public static class Metadata {

        public final String title;
        public final String version;
        public final String vendor;

        @SuppressWarnings("unused")
        public Metadata() {
            this.title = null;
            this.version = null;
            this.vendor = null;
        }

        public Metadata(Package pkg) {
            this.title = pkg.getImplementationTitle();
            this.version = pkg.getImplementationVersion();
            this.vendor = pkg.getImplementationVendor();
        }

    }

}
