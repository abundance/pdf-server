package com.abundance.pdf.runtime;

import com.abundance.pdf.resources.GeneratorResource;
import com.abundance.pdf.runtime.resources.MetadataResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class RunApplication extends Application<RunConfiguration> {

    public static void main(String[] args) throws Exception {
        new RunApplication().run(args);
    }

    @Override
    public String getName() {
        return "pdf-server";
    }

    @Override
    public void initialize(Bootstrap<RunConfiguration> bootstrap) {
        // nothing to do yet
    }

    @Override
    public void run(RunConfiguration configuration, Environment environment) {
        environment.jersey().register(new MetadataResource());

        environment.jersey().register(new GeneratorResource());
    }

}
