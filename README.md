Example

1. Store desired HTML into test.html
1. Start the server with: mvn exec:exec
1. Render the PDF with: curl -d "@test.html" http://localhost:8188/api/template/render > test.pdf

Releasing

Run: mvn -Darguments="-DskipTests -Dmaven.javadoc.skip=true" release:prepare

This does the following things:

* Prompt user for release version info (name, number, next x-SNAPSHOT)
* Check that there are no uncommitted changes in the source
* Check that there are no SNAPSHOT dependencies
* Change the version in the POMs from x-SNAPSHOT to a new version
* Transform the SCM information in the POM to include the final destination of the tag
* Run the project tests against the modified POMs to confirm everything is in working order
* Commit the modified POMs
* Tag the code in the SCM with a version name
* Bump the version in the POMs to a new value y-SNAPSHOT
* Commit the modified POMs
